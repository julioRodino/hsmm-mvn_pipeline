% For trial selection it asumes all subjects have the same length

% Dopamine Modulation experiment sourcemodel generation
addpath('fieldtrip-20210914')
clear all
run 'eeglab2019_0/eeglab.m'
close all
%% Initialize fieldtrip
ft_defaults

%% Load Global Data

% HMM-DOPAMINE project Headmodel and Electrodes
headmodel = ft_read_headmodel('headmodel_eeg.mat');
load('elec_aligned.mat');
load('D2_leadfield.mat');
elec_aligned = elec_aligned.elec_aligned;
elec = elec_aligned;
atlas = ft_read_atlas('dkatlas.mat');
load('sourcemodel.mat');

% Load HsMM state sequence
load('output/HsMM_output_MVN_sensor_multisubject_fs_64_f4-8_D2.mat')
stateseq = training_output.out_hsmm.stateseq;
training_fs = GEEG.fs_new;
n_states = training_output.out_hsmm.nstates;
clear training_output GEEG
% %% Prepare sourcemodel
% cfg             = [];
% cfg.elec        = elec;
% cfg.headmodel   = headmodel;
% 
% cfg.warpmni     = 'yes';
% cfg.resolution  = 10;
% cfg.inwardshift = -1;
% sourcemodel     = ft_prepare_sourcemodel(cfg);
%%
% cfg         = [];
% cfg.method  = 'interactive';
% cfg.headshape = headmodel.bnd(1);
% cfg.elec    = elec;
% elec_aligned = ft_electroderealign(cfg);
% % %Rotate:    0    5      -90
% % %scale:     1 0.95   1
% % %translate: 25       0   43

%%
% map source locations onto an anatomical label in an atlas
cfg = [];
cfg.interpmethod = 'nearest';
cfg.parameter = 'tissue';
sourcemodel2 = ft_sourceinterpolate(cfg, atlas, sourcemodel);

% %% visualize the coregistration of electrodes, headmodel, and sourcemodel.
% % create colormap to plot parcels in different color
% nLabels     = length(atlas.tissuelabel);
% colr        = hsv(nLabels); 
% vertexcolor = ones(size(atlas.pos,1), 3);
% for i = 1:length(atlas.tissuelabel)
%     index = find(atlas.tissue==i);
%    if ~isempty(index) 
%       vertexcolor(index,:) = repmat(colr(i,:),  length(index), 1);
%    end   
% end
% 
% % make the headmodel surface transparent
% ft_plot_headmodel(headmodel, 'edgecolor', 'none','facecolor', 'black'); alpha 0.1
% ft_plot_mesh(atlas, 'facecolor', 'brain',  'vertexcolor', vertexcolor, 'facealpha', .5);
% %ft_plot_sens(elec_aligned, 'label', 'yes');
% ft_plot_mesh(sourcemodel, 'vertexcolor', 'm')
% view([0 -90 0])

%% Data files
file_list = dir('data/*.set');
%%
file        = file_list(1,1).name;
folder      = file_list(1,1).folder;
data_path   = append(folder,'/',file);
output_path = 'output/beamformer';
status = mkdir(output_path);
%% Preprocess
cfg     = [];
cfg.dataset      = data_path;
cfg.continuous   = 'yes';
cfg.detrend      = 'yes';
data_eeg = ft_preprocessing(cfg);
data_eeg.elec = elec_aligned;

%% Timelock 
cfg                  = [];
cfg.covariance       = 'yes';
cfg.removemean       = 'yes';
all_data             = ft_timelockanalysis(cfg, data_eeg);
%%    
for i = 2:size(file_list,1)
    file        = file_list(i,1).name;
    folder      = file_list(i,1).folder;
    data_path   = append(folder,'/',file);
    output_path = 'output/beamformer';
    status = mkdir(output_path);

    %% Preprocess
    cfg     = [];
    cfg.dataset      = data_path;
    cfg.continuous   = 'yes';
    cfg.detrend      = 'yes';
    data_eeg = ft_preprocessing(cfg);
    data_eeg.elec = elec_aligned;
    %% Timelock 
    cfg                  = [];
    cfg.covariance       = 'yes';
    %cfg.keeptrials       = 'yes';
    cfg.removemean       = 'yes';
    timelock             = ft_timelockanalysis(cfg, data_eeg);
    %% Append all subjects
    cfg = [];
    all_data = ft_appenddata(cfg, all_data, timelock);
    
end

%% Preprocessing or standarization
% filter data
cfg            = [];
cfg.bpfilter   = 'yes';
cfg.bpfilttype = 'firws';
cfg.bpfreq     = [1 40];
cfg.dftfilter  = 'yes';
cfg.demean     = 'yes';
cfg.reref      = 'yes';
cfg.refchannel = 'all';
cfg.refmethod  = 'avg';
all_data       = ft_preprocessing(cfg,all_data);

%% Segment data (all_trial) into states

time = linspace(0,(size(stateseq,2)/training_fs)-1,size(stateseq,2));
statemat = zeros([n_states size(stateseq,2)]);
for ii = 1:n_states
    statemat(ii,:) = (stateseq == ii);
end

all_trial = cell2mat(all_data.trial);
all_time  = linspace(0,(size(all_trial,2)./all_data.fsample)-1,size(all_trial,2));
time_trFs = linspace(0,(size(stateseq,2)./training_fs)-1,size(stateseq,2));
% Change fs of stateseq
nw_stateseq = zeros([1, size(all_trial,2)]);
for ii = 1:size(all_trial,2)
    t = all_time(ii);
    state_indx = find(t >= time_trFs,1,'last');
    state = stateseq(state_indx);
    nw_stateseq(ii) = state;
    %states_data{state} = horzcat(states_data{state}, all_trial(:,ii));
    disp(['Changing stateseq fs: ' num2str(ii/size(all_trial,2) * 100) '%']);
end

% % Plot old and new stateseq 
% fs_ratio = all_data.fsample / training_fs;
% plot(time_trFs(1:int8(fs_ratio*1000)),stateseq(1:int8(fs_ratio*1000))); hold on
% plot(all_time(1:1000),nw_stateseq(1:(1000)))

% Create cell array for data and indexes
for ii = 1:n_states
    states_data{ii} = [];
    states_indx{ii} = [];
end

% Segment into states
for state = 1:n_states
    indx = find(nw_stateseq == state);
    states_indx{state} = indx;
    states_data{state} = all_trial(:,indx);
    disp(['Segmenting into states: ' num2str(state) '/' num2str(n_states)])
end


%% Create fieldtrip structure for each state
states_struct = {};
for i = 1:n_states
    states_struct{i} = all_data;
    states_struct{i}.trial  = {states_data{i}};
    states_struct{i}.time   = {linspace(1,size(states_data{i},2)/all_data.fsample,size(states_data{i},2))};
end
%%


%% Do source reconstruction over each separated concatenated states data
source_mat = {};
for i = 1:size(states_struct,2)
    % Define in which state we are going to work in
    state_ft_struct = states_struct{i};
    
    %% Timelock 
    cfg                  = [];
    cfg.covariance       = 'yes';
    cfg.removemean       = 'yes';
    timelock             = ft_timelockanalysis(cfg, state_ft_struct);
    
%     %% calculate leadfield
%     % Uses OpenMEEG binaries. Remember adding to system path
%     % OpenMEEG ver 2.4.9999 does not work. Use previous versions
%     cfg                  = [];
%     cfg.elec             = elec_aligned;  % Electrode distances
%     cfg.channel          = elec_aligned.label;
%     cfg.sourcemodel.pos = sourcemodel.pos;              % 2002v source points
%     cfg.sourcemodel.inside = 1:size(sourcemodel.pos,1); % all source points are inside of the brain
%     cfg.headmodel        = headmodel;   % volume conduction headmodel
%     lf                   = ft_prepare_leadfield(cfg);
    
    %% create spatial filter using the lcmv beamformer
     % Source structure is 6GB. Unless you have to analyze the sources,
     % save the lf and timelock variables which will allow you to do the
     % source reconstruction.

    cfg                    = [];
    cfg.channel            = lf.label;
    cfg.elec               = elec_aligned;
    cfg.method             = 'lcmv';
    cfg.grid               = lf; % leadfield
    cfg.headmodel          = headmodel; % volume conduction model (headmodel)
    cfg.lcmv.keepfilter    = 'yes';
    cfg.lcmv.fixedori      = 'yes';
    cfg.lcmv.projectnoise  = 'yes';
    cfg.lcmv.weightnorm    = 'nai';
    cfg.lcmv.lambda        = '5%';
    cfg.atlas            = sourcemodel2;
    source               = ft_sourceanalysis(cfg, timelock);    %% Save labeld sources time series
    %%
    source.avg.mom2 = cell2mat(source.avg.mom);
    
    cfg           = [];
    cfg.parameter = 'tissue';
    cfg.interpmethod = 'nearest';
    dkatlas2  = ft_sourceinterpolate(cfg, atlas, source);

    cfg           = [];
    cfg.parameter = 'mom2';
    sourceint  = ft_sourceinterpolate(cfg,source,dkatlas2);
    sourceint  = ft_sourceparcellate([],sourceint, dkatlas2);
    

    %% Save
    labels = dkatlas2.tissuelabel;
    eeg_rest = sourceint.mom2;
    source_mat{i} = eeg_rest;
    
end
%% Restore to source x time instead of states x source x time
% Segment into states
source_data = zeros([size(source_mat{1},1), size(all_trial,2)]);
for state = 1:n_states
    source_data(:,states_indx{state}) = source_mat{state};
    %states_data{state} = all_trial(:,indx);
    disp(['Segmenting into states: ' num2str(state) '/' num2str(n_states)])
end
save('output/beamformer/source_data_D2_4-8.mat','all_data','nw_stateseq','source_data','source_mat','statemat','-v7.3')


