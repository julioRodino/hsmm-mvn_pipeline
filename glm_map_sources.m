% glm_map_sources.m
% Script to perform GLM to project maps according to states sequences.
% 
% Filter and standardise data before GLM
% *** This script belongs to the HSMM research. ***
% 
% ALAND ASTUDILLO & JULIO RODI�O - APRIL 2022

%% GET TOOLBOXES
addpath('spm12');
addpath(genpath('HMM-MAR-master 20180107'));
%% Load Data
load('output/beamformer/source_data_D2_4-8.mat')
load('T36.mat')
%% define the variables to use and parameters
n_states = 7;
n_sources = size(source_data,1);
fs = 500;
fs_new = 250;

%% Resample Source Data

[data, T2] = downsampledata(source_data', cell2mat(T), fs_new, fs);
source_data = data';
clear data
%% Generate Statemat
% Time and resampled time vector
time = linspace(0,(size(nw_stateseq,2)./fs)-1,size(nw_stateseq,2));
time_new = linspace(0,(size(source_data,2)./fs_new)-1,size(source_data,2));
% Allocating resampled sequence of states
stateseq2 = zeros([1, size(source_data,2)]);
parfor ii = 1:size(source_data,2)
    %t = time_new(ii);
    state_indx = find(time_new(ii) >= time,1,'last');
    state = nw_stateseq(state_indx);
    stateseq2(ii) = state;
    disp(['Changing stateseq fs: ' num2str(ii/size(source_data,2) * 100) '%']);
end
disp('Changing stateseq fs: Completed...');
% %% Plot states sequences
% plot(time, nw_stateseq); hold on
% plot(time_new, stateseq2);
% xlim([100 120])

%% Create statemat
statemat = zeros([n_states size(stateseq2,2)]);
for ii = 1:n_states
    statemat(ii,:) = (stateseq2 == ii);
end
%% Segment into subjects (sessions) based on T matrix
% Allocate cell arrays for state sequence and source data
s_statemat = cell(1,size(T2,2));
s_source_data = cell(1,size(T,2));

T_sum = cumsum(T2); % cumulative sum

s_source_data{1} = source_data(:,1:T_sum(1));
s_statemat{1} = statemat(:,1:T_sum(1));
for t = 2:size(T2,2)
    s_statemat{t}       = statemat(:,T_sum(t-1)+1:T_sum(t));
    tmp                 = source_data(:,T_sum(t-1)+1:T_sum(t));
    tmp                 = filterdata(tmp',T2(t),fs,[1,30])';
    tmp                 = standardisedata(tmp',T2(t),1)';
    s_source_data{t}    = tmp;
end
clear tmp
%% Start Loop for calculating B for each subject
% Allocate memory for betas
s_beta = zeros([size(T2,2), n_states, n_sources]);
% Allocate memory for weights
s_weight = cell(1, size(T2,2));
for i = 1:size(T2,2)
    %% Define X and Y for GLM
    X = s_statemat{i}';
    Y = s_source_data{i}';
    %% GLM WITH SPM: SPM-GLM OPTIMIZED
    [B, W] = spm_robust_glm(Y, X, 1, 3);
    s_beta(i,:,:) = B;
    s_weight{i} = W;
end
%% Save betas and weights
save('output/GLM/GLM_betas_f4-8_D2.mat','s_beta','s_weight','stateseq2','-v7.3')
