%%%%%%%%%%%%%%%%%%%%%%%
% By Roee Lahav 2009  %
% roeela@bgu.ac.il    %
%%%%%%%%%%%%%%%%%%%%%%%

Graph1=[];
Graph1.A = [2 2 0 ; 
            0 0 2 ; 
            1 0 2 ];
Graph1.EdgeLabels{1,1}=['a';'b'];
Graph1.EdgeLabels{1,2}=['c';'d'];
Graph1.EdgeLabels{2,3}=['e';'f'];
Graph1.EdgeLabels{3,1}=['a'];
Graph1.EdgeLabels{3,3}=['e';'f'];
Graph1.X=[0 0.5 1];
Graph1.Y=[0 sin(pi/6) 0];
Graph1.StateLabels={'\alpha';'\beta';'\gamma'};

% cla;
figure;
DirectGPlot(Graph1)
% pause;

% x=Franchek(Graph1.A,3,[2 2 1]')


%% First round of splitting

Partition=cell(3,3);
Partition{1,1}=[2;1];
Partition{1,2}=[1;2];
Partition{2,3}=[1;1];
Partition{3,1}=[1];
Partition{3,3}=[1;1];

Graph2=StateSplitRound(Graph1,Partition);
Graph2.X=[0 -0.1 0.5 1 ];
Graph2.Y=[0 0.1 sin(pi/6) 0 ];
cla;
DirectGPlot(Graph2);
pause;
x=Franchek(Graph2.A,3,2*[1 1 1 1]')

%% Second round of splitting

Partition=cell(4,4);
Partition{1,1}=1;
Partition{1,2}=1;
Partition{1,3}=1;
Partition{2,1}=1;
Partition{2,2}=1;
Partition{2,3}=1;
Partition{3,4}=[1;1];
Partition{4,1}=1;
Partition{4,2}=2;
Partition{4,4}=[2;1];
Graph3=StateSplitRound(Graph2,Partition);
x=Franchek(Graph3.A,3,[ 1 1 1 1 1]')

Graph3.X=[0 -0.1 0.5 1 1.2 ];
Graph3.Y=[0 0.1 sin(pi/6) 0 0.2];
cla;
DirectGPlot(Graph3);
pause;

CheckAnt(Graph3)


