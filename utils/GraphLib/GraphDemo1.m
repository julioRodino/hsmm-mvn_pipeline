%%%%%%%%%%%%%%%%%%%%%%%
% By Roee Lahav 2009  %
% roeela@bgu.ac.il    %
%%%%%%%%%%%%%%%%%%%%%%%
Graph1.A=...
   [0 1 0 0 0 0 0 0 ;
    0 0 1 0 0 0 0 0 ;
    0 0 0 1 0 0 0 0 ;
    0 0 0 0 1 0 0 0 ;
    0 0 0 1 0 1 0 0 ;
    0 0 1 0 0 0 1 0 ;
    0 1 0 0 0 0 0 1;
    2 0 0 0 0 0 0 0];

Graph1.EdgeLabels=cell(8,8);
Graph1.EdgeLabels{1,2}=['0'];
Graph1.EdgeLabels{2,3}=['0'];
Graph1.EdgeLabels{3,4}=['0'];
Graph1.EdgeLabels{4,5}=['0'];
Graph1.EdgeLabels{5,6}=['0'];
Graph1.EdgeLabels{6,7}=['0'];
Graph1.EdgeLabels{7,8}=['0'];
Graph1.EdgeLabels{8,1}=['0';'1'];
Graph1.EdgeLabels{7,2}=['1'];
Graph1.EdgeLabels{6,3}=['1'];
Graph1.EdgeLabels{5,4}=['1'];

Graph1.StateLabels{1}='u_1';
Graph1.StateLabels{2}='u_2';
Graph1.StateLabels{3}='u_3';
Graph1.StateLabels{4}='u_4';
Graph1.StateLabels{5}='u_5';
Graph1.StateLabels{6}='u_6';
Graph1.StateLabels{7}='u_7';
Graph1.StateLabels{8}='u_8';

Graph1.X=[1 2 3 4 4 3 2 1];
Graph1.Y=[1 1 1 1 2 2 2 2];


% cla;
figure;
DirectGPlot(Graph1);
% pause;


%% Find A^2, G^2 and (A^2,4) EigenVector

p=1;
q=2;

Graph2=FindPowerGraph(Graph1,q);
cla;
DirectGPlot(Graph2);
pause;
Xi1=2*ones(8,1);
x2=Franchek(Graph2.A,2^p,Xi1);


%% Eliminate 0 weighted states

[Graph3,x3]=EliminateStates(Graph2,x2);
cla;
DirectGPlot(Graph3)
pause;
% just checking...
testx=Franchek(Graph3.A,2^p,x3);

%% Make encoder in one round of splitting

Partition=cell(4,4);

Partition{1,2}=1;
Partition{2,2}=1;
Partition{2,3}=2;
Partition{3,1}=1;
Partition{3,2}=2;
Partition{3,4}=1;
Partition{4,1}=[1;1];

Graph4=StateSplitRound(Graph3,Partition);

Graph4.X=[1 2.8 3.2  2.8 3.2 1];
Graph4.Y=[1 0.8 1.2 3.2   2.8 3];
cla;
DirectGPlot(Graph4)
pause;
x4=Franchek(Graph4.A,2^p,ones(6,1))

%% Make encoder in two rounds of splitting

Partition=cell(4,4);

Partition{1,2}=1;
Partition{2,2}=1;
Partition{2,3}=1;
Partition{3,1}=1;
Partition{3,2}=2;
Partition{3,4}=1;
Partition{4,1}=[1;1];

Graph5=StateSplitRound(Graph3,Partition);

Graph5.X=[1 2.8   2.8 3.2 1];
Graph5.Y=[1 0.8  3.2   2.8 3];

cla
DirectGPlot(Graph5)
pause;
x5=Franchek(Graph5.A,2^p,2*ones(5,1))
%  Round 2
Partition=cell(5,5);
Partition{1,2}=1;
Partition{2,2}=2;
Partition{2,3}=1;
Partition{2,4}=1;
Partition{3,1}=1;
Partition{3,5}=1;
Partition{4,2}=1;
Partition{5,1}=[1;1];


Graph6=StateSplitRound(Graph5,Partition);
Graph6.X=[1 2.8 3.2  2.8 3.2 1];
Graph6.Y=[1 0.8 1.2 3.2   2.8 3];

cla;
DirectGPlot(Graph6)
pause;
x6=Franchek(Graph6.A,2^p,ones(6,1))
%% create higher anticipation encoder by use of moore co form


Graph7=FindMooreCoForm(Graph6);

Graph7.X=rand(1,12);
Graph7.Y=rand(1,12);
cla
DirectGPlot(Graph7);
pause;
CheckAnt(Graph7)


