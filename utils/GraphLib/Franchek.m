function x=Franchek(A,n,xi)
%%%%%%%%%%
% x=Franchek(A,n,xi)
% 
% This function runs the Franchek algorithm for finding the (A,n)
% approximate eigenvector inside radius xi. It returns x=0 if there is no
% such vector otherwise the vector with largest entries is returned.
%
% the (A,n) approximate eigenvector is the integer vector x which satisfies
%   Ax >= nx
%
%%%%%%%%%%%%%%%%%%%%%%%
% By Roee Lahav 2009  %
% roeela@bgu.ac.il    %
%%%%%%%%%%%%%%%%%%%%%%%


dim=length(xi);
y=xi;
x=zeros(dim,1);

while (any(x~=y))
    x=y;
    y=min(floor(A*x/n),x);
end;