function res = plotTransMat2(tranMatc122,umbral,factor,radio,fsize,xy,labelList,colorList);
% INPUTS:
%--------------------------------
% umbral = 5;
% factor = 1;
% radio = 0.8; %0.4; % define radious for circles
% plotauto = 0;
% fsize = 12; % define fontsize for states names
% xy : define positions of centroids for each circle
% labelList
% colorList
%--------------------------------

nEstados = size(tranMatc122,1);

aVert2 = [];
ancho2 = [];
colVert = {};
cont = 1;
for k1 = 1:nEstados
    for k2 = 1:nEstados
        if k1~=k2 % no en la diagonal, solo interacciones entre estados
            ancho2(cont) = tranMatc122(k1,k2);
%             r = -0.1 + (0.1+0.1)*rand(1);
            r =0;
            aVert2(cont,:) = [xy(k1,:),xy(k2,:)+r];
            colVert{cont} = colorList{k1};
            cont = cont+1;
        end
    end
end

%  ANCHO DEFAULT: 2;
% ancho1(1) = tranMatc122(1,2); % S1 TO S2 (1,2)
% ancho1(2) = tranMatc122(2,1); % S2 TO S1 (2,1)
% 
% ancho1(3) = tranMatc122(4,1); % S4 TO S1 (4,1)
% ancho1(4) = tranMatc122(1,4); % S1 TO S4 (1,4)
% 
% ancho1(5) = tranMatc122(3,2); % S3 TO S2 (3,2)
% ancho1(6) = tranMatc122(2,3); % S2 TO S3 (2,3)
% 
% ancho1(7) = tranMatc122(4,3); % S4 TO S3 (4,3)
% ancho1(8) = tranMatc122(3,4); % S3 TO S4 (3,4)
% 
% ancho1(9) = tranMatc122(4,2); % S4 TO S2 (4,2)
% ancho1(10) = tranMatc122(2,4); %S2 TO S4 (2,4)
% 
% ancho1(11) = tranMatc122(3,1); %S3 TO S1 (3,1)
% ancho1(12) = tranMatc122(1,3); %S1 TO S3 (1,3)

% cirVert = [0, 0;
%            5, 0;
%            5, 5;
%            0, 5];
cirVert = xy;

% aVert = [     1,-0.5,   4,-0.5; % S1 TO S2 (1,2)
%               4, 0.5,   1, 0.5; % S2 TO S1 (2,1)
%            -0.5,   4,-0.5,   1;
%             0.5,   1, 0.5,   4;
%             4.5,   4, 4.5,   1;
%             5.5,   1, 5.5,   4;
%               1, 4.5,   4, 4.5;
%               4, 5.5,   1, 5.5;
%             0.7,   4,   4, 0.7;
%             4.3,   1,   1, 4.3;
%               4, 4.3, 0.7,   1;
%               1, 0.7, 4.3,   4];

hold on;
%------------------------------
% PLOT ARROWS    
%--------------------------------
% for k1=1:length(ancho1)
% if ancho1(k1)>umbral
% plot_arrow(aVert(k1,1),aVert(k1,2), aVert(k1,3),aVert(k1,4),...
%     'linewidth',ancho1(k1),'headwidth',0.07,'headheight',0.15 ,...
%     'color',colorList{1},'facecolor',colorList{1},'edgecolor',colorList{1}); %blue S1 TO S2
% end
% end
%--------------------------------
% parameters for curve arrow: 
angle = 170; % Anglebetween start and end of arrow
direction = 1; % for CW enter 1, for CCW enter 0
head_size = 10; % Arrow head size

%--------------
for k1=1:length(ancho2)
if ancho2(k1)>umbral
    x1 = aVert2(k1,1);
    y1 = aVert2(k1,2);
    x2 = aVert2(k1,3)+(-1)*aVert2(k1,3)*0.2;
    y2 = aVert2(k1,4)+(-1)*aVert2(k1,4)*0.2;
arrowHandles = plot_arrow(x1,y1, x2,y2,...
    'linewidth',ancho2(k1)*factor,'headwidth',0.07,'headheight',0.15 ,...
    'color',colVert{k1},'facecolor',colVert{k1},'edgecolor',colVert{k1}); %blue S1 TO S2

% alpha(arrowHandles(1),0.7);
alpha(arrowHandles(2),0.7);
% alpha(0.7);
%-----------------------------curve arrow: 
% radius = (sqrt((x2-x1)^2+(y2-y1)^2))/2;
% m = (y2-y1)/(x2-x1);
% arrow_angle = atand(m)+90;
% colour = colVert{k1}; %'k'; % Colour of arrow
% centre = [x1+x2,y1+y2]/2; %[0 0];
% circular_arrow(gca, radius, centre, arrow_angle, angle, direction, colour, head_size);
%------------------------
end
end





%--------------------------------
% PLOT CIRCLES
%--------------------------------
for k=1:nEstados
cir{k} = circles(cirVert(k,1),cirVert(k,2),radio,'Color',colorList{k});
alpha(0.7);
end

distText = 0.1;
for k=1:nEstados
text(cirVert(k,1)-distText,cirVert(k,2),labelList{k},'FontWeight','bold','FontSize',fsize); % Texto state 1: 0,0
end
% axis([-2 7 -2 7]);
% axis square; axis off

res = 1;

