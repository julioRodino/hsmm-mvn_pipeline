% plotConnectivityModelResults

% resort model results
outhsmm = model_output.outhsmm;
sim_arrayhsmm = model_output.sim_arrayhsmm;
modelhsmm = model_output.modelhsmm;
hsmmmodel1 = model_output.hsmmmodel1;

%% Plot Fre energy evolution
figure;
plot([outhsmm.fedetail.fe])
xlabel('Iteration', 'FontSize', 12);
ylabel('FE value', 'FontSize', 12);
set(gca, 'FontSize', 12); 
set(gcf, 'color', 'w');

%% Plot State sequences and compare
[seq2, new_states_position] = util.renameseq(stateseq(4:n_points + 3), outhsmm.stateseq);

n_points_show = 1000;

figure;
subplot(2,1,1);
bar(stateseq(1:n_points_show));
subplot(2,1,2);
bar(seq2(1:n_points_show));
ylabel('State');
xlabel('Time');
set(gcf, 'color', 'w');

%% Plot estimated conecctivity values 
for jstate = 1:3
    [aux, w2{jstate}] = util.convertermar(hsmmmodel1.emis_model.posterior.coef_normal{new_states_position(jstate, 2)}.mean, n_sources, max_lag, mask); %estimados
end

figure;
for kstate = 1:3
	for kplot = 1:3
        subplot(3,3,(kstate-1)*3 + kplot);
        imagesc(w2{kstate}(:, :, kplot));
        %colormap('spring')
        colorbar;
        if kplot==1
        	pal = ['State: ', num2str(kstate), '  Lag: ', num2str(kplot)];
        else
        	pal = ['Lag: ', num2str(kplot)];
        end
        title(pal);
	end
end
