% plotConnections
figure;
for kstate = 1:n_states_data
    for kp = 1:3
        subplot(3,3,(kstate - 1) * 3 + kp); 
        imagesc(AR{kstate}{kp});
        %colormap('spring')
        colorbar
        if kp==1
            pal = ['State: ', num2str(kstate), '  Lag: ', num2str(kp)];
        else
            pal = ['Lag: ' num2str(kp)];
        end
        title(pal)
    end
end
