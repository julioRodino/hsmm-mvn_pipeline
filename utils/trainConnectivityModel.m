function model_output = trainConnectivityModel(data, model_options)

n_states = model_options.n_states; %State Number for training
mask = model_options.mask; %Structural Constrains
n_sources = model_options.n_sources;
max_lag = model_options.max_lag;

max_iter_sim = model_options.max_iter_sim;
tol = model_options.tol;
max_cyc = model_options.max_cyc;

%% construct and train the model
emisionModel = emis_model.mar3(n_sources, n_states, max_lag);  %Emission Model
hsmmmodel1 = hsmm(n_sources, n_states, emisionModel);
hsmmmodel1.priornoinf();
emisionModel.prior.coef_mask = mask; 

%% train the model
[outhsmm, sim_arrayhsmm, modelhsmm] = hsmmmodel1.train(data,... 
                                                       'maxitersim', max_iter_sim,...
                                                       'tol', tol,...
                                                       'maxcyc', max_cyc);

model_output.outhsmm = outhsmm;
model_output.sim_arrayhsmm = sim_arrayhsmm;
model_output.modelhsmm = modelhsmm;
model_output.hsmmmodel1 = hsmmmodel1;
