function transform_output = transformData(data_container, tr_options)
% FUNCTION EXPLANATION
% INPUTS
% OUTPUTS
% ALAND ASTUDILLO FEB 2021

% tr_options :  fs_new, n_components, do_hilbert, filter_band

% % get % Oxford's toolbox for data transformation only
% addpath(genpath('HMM-MAR-master 20180107')); 
% run 'eeglab2019_0/eeglab.m'

data_cell{1} = data_container.data_prepro;
T_cell = data_container.T;

% define transformation options and parameters:
clear transform_options
transform_options.K = 4; % arbitrary, it does not affect HsMM's K values
transform_options.Fs = data_container.fs; 
transform_options.filter = tr_options.filter_band; 
transform_options.detrend = 1;
transform_options.standardise = 1;
transform_options.leakagecorr = 0 ;
transform_options.onpower = tr_options.do_hilbert; % Hilbert's transform
transform_options.leida = 0; % Leading Phase Eigenvectors 
transform_options.pca = tr_options.n_pca_components; %0; 
transform_options.standardise_pc = 1;
transform_options.downsample = tr_options.fs_new;
transform_options.order = 0;
transform_options_original = transform_options; % for keep legacy
do_log = tr_options.do_log; % rescaling using simple Log transformation
do_post_ref = 1; % average re-reference after log, uses the reref command from Eeglab!
mantain_steps = 1;

[data_transformed, T3ds, transform_options2, data_steps] = hmmpreprocess2(data_cell, T_cell, transform_options, mantain_steps, do_log, do_post_ref);

n_dimensions = size(data_transformed.X,2); % same as n_components
% scaleFactor = mean(diag((inv(cov(data_transformed.X)))))/n_dimensions/20; % optional

%% ------------------------  DEFINE THE DATA STRUCTURE GEEG ------------------------ 
data_prepared = data_steps{1, 8}.D.X; % final transformed data
data_inter = data_steps{1, 4}.D.X'; % data envelope
T_sum = cumsum(T3ds);

GEEG = struct;
GEEG.data_prepared = data_prepared; %[time points x components]
GEEG.data_inter = data_inter; %[channels x time points]
GEEG.T = T3ds; % total time points
GEEG.T_sum = T_sum; % total time points (all blocks for multiblock purpuses)
GEEG.n_dimensions = n_dimensions;
GEEG.fs_new = tr_options.fs_new;
GEEG.fs = data_container.fs; % original fs
GEEG.transform_options_original = transform_options_original;
GEEG.transform_options2 = transform_options2;
GEEG.chanlocs = data_container.chanlocs;
%GEEG.chanlocs3 = data_container.chanlocs3;

transform_output.GEEG = GEEG;
transform_output.T3ds = T3ds;
transform_output.transform_options = transform_options;
transform_output.transform_options2 = transform_options2;
transform_output.data_transformed = data_transformed; 
transform_output.data_steps = data_steps;
transform_output.do_log = do_log; % rescaling using simple Log transformation
transform_output.do_post_ref = do_post_ref; % average re-reference after log, uses the reref command from Eeglab!
transform_output.mantain_steps = mantain_steps;

