% gamma2seq
function seq = gamma2seq(gamm)

seq = zeros(1,size(gamm,1));
for kpoint =1:size(gamm,1)
    g1 = gamm(kpoint,:);
    [maxVal,maxPos] = max(g1);
    seq(1,kpoint) = maxPos;
end