function N = MSS_tutorial(AR, hsmm3, structural_constraint, m)

N = zeros(3,3);

for i=1:3
    [mask] = util.convertermar(structural_constraint, 10, 3);
    current_AR= AR{i};
    structural_simulated = zeros(10,10,3);
    
    for j=1:3
        structural_simulated(:,:,j) =current_AR{j};
    end
    structural_simulated = util.convertermar(structural_simulated, 10, 3,mask);
    
    for k=1:3
        w=util.convertermar(hsmm3.emis_model.posterior.coef_normal{m(k,2)}.mean,10,3,mask);
        N(i,k)= 1 - (norm( w-structural_simulated,'fro')/(norm(w,'fro')+norm(structural_simulated,'fro')));       
    end
end


label = {'DMN', 'VN', 'SMN'};
figure('Renderer', 'painters', 'Position', [10 10 400 300])
imagesc(N);  %axis off
colorbar
        
for i = 1:size(N)
    t = text(i, i, num2str(round(N(i,i),2)) , 'FontSize',12);
    set(t,'HorizontalAlignment', 'center');

end

% xticks([1:1:3])
% xticklabels(label)
% yticks([1:1:3])
% yticklabels(label)

set(gca,'fontweight','bold','fontsize',12);
xlabel('Estimated networks', 'FontSize',15);
ylabel('Simulated networks', 'FontSize',15);
%title('MSS entre las RSNs')

end