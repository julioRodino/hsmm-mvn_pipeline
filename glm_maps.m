% glm_maps_example2
% Script to perform GLM to project maps according to states sequences.
% 
% *** This script belongs to the HSMM research. ***
% 
% ALAND ASTUDILLO - APRIL 2022

%% GET TOOLBOXES
addpath('D:\spm12Downloaded\spm12');

% run eeglab

%% define the variables to use and parameters
chosen_trial = 1;

ADATA = all_data.trial{chosen_trial};

n_channels = size(ADATA, 1);
n_points = size(ADATA, 2);

state_sequence = statemat;

fs = 500;
fs_new = 64;

%% define useful variables
block_size = 200001; % to avoid "out of memory" problems
Yg = zscore(ADATA); % FIRST BLOCK

% get the secuence from the model
state_sequence_resamp = resample(state_sequence', fs, fs_new, 2); % resample to original srate
state_sequence_resamp = round(state_sequence_resamp); % altered version of the sequence. problem at borders level

% select a portion of the sequence:
state_seq = state_sequence_resamp(0 + 1:0 + block_size, :); 
Zmat = state_seq;

data_percent = block_size/length(state_sequence_resamp) * 100; % percent of used data for GLM

Xmat = Zmat(1:block_size, :); 

%% define a miniblock for the GLM example
mini_block = 15000;

Yg2 = Yg(:, 1:mini_block)'; 
Xmat2 = Xmat(1:mini_block, :);

%% GLM WITH SPM: SPM-GLM OPTIMIZED
[B, W] = spm_robust_glm(Yg2, Xmat2, 1, 3);

%% FIGURE: PLOT GLM MAPS USING CORRELATIONS AS REGRESORS: SPM-GLM
n_states = 7;

chanlocs = 'D:\EEG_Grace_oddball\data\GraceHope.locs'; % define a eeglab chanlocs structure 

n_rows = floor((n_states + 2)/3);

figure;
for kstate = 1:n_states
  subplot(3,n_rows,kstate); 
  topoplot(B(kstate, :), chanlocs, 'electrodes', 'off'); %, 'maplimits', escala);
  title(['GLM2 S', num2str(kstate)], 'FontSize', 14);  
  colorbar; caxis('auto');
end
colormap redblue;
set(gcf, 'color', 'w');
