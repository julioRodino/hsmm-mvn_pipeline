% standardize z-score per subject. Options are 1 (do) or 0 (Don't)

function [T concatenated_sources_D2]=subjcat(files, standardize)
concatenated_sources_D2 = [];
T = [];
    for ii = 1:size(files,1)
        fileName = files(ii).name;
        fileFolder = files(ii).folder;
        load(strcat(fileFolder,'/',fileName), 'eeg_rest');
        disp(['Loaded: ' files(ii).name])
        if standardize
            data.X = eeg_rest';
            t = 1:size(eeg_rest,2);
            data.X(t,:) = bsxfun(@minus,data.X(t,:),mean(data.X(t,:))); 
            sdx = std(data.X(t,:));
            data.X(t,:) = bsxfun(@rdivide,data.X(t,:),sdx);
            concatenated_sources_D2 = horzcat(concatenated_sources_D2,data.X');
        else
            concatenated_sources_D2 = horzcat(concatenated_sources_D2,eeg_rest);
        end
        T{ii} = size(data.X,1);
        
    end
end
% V2 of subjcat
