load('output/beamformer/source_data_D2_4-8.mat');
load('T36.mat')

%% define the variables to use and parameters
n_states = 7;
n_sources = size(source_data,1);
fs = 500;
fs_new = 250;

%% Resample Source Data

[data, T2] = downsampledata(source_data', cell2mat(T), fs_new, fs);
source_data = data';
clear data
%% Generate Statemat
% Time and resampled time vector
time = linspace(0,(size(nw_stateseq,2)./fs)-1,size(nw_stateseq,2));
time_new = linspace(0,(size(source_data,2)./fs_new)-1,size(source_data,2));
% Allocating resampled sequence of states
stateseq2 = zeros([1, size(source_data,2)]);
parfor ii = 1:size(source_data,2)
    %t = time_new(ii);
    state_indx = find(time_new(ii) >= time,1,'last');
    state = nw_stateseq(state_indx);
    stateseq2(ii) = state;
    disp(['Changing stateseq fs: ' num2str(ii/size(source_data,2) * 100) '%']);
end
disp('Changing stateseq fs: Completed...');
% %% Plot states sequences
% plot(time, nw_stateseq); hold on
% plot(time_new, stateseq2);
% xlim([100 120])

%% Create statemat
statemat = zeros([n_states size(stateseq2,2)]);
for ii = 1:n_states
    statemat(ii,:) = (stateseq2 == ii);
end
%% Segment into subjects (sessions) based on T matrix
% Allocate cell arrays for state sequence and source data
s_statemat = cell(1,size(T2,2));
s_source_data = cell(1,size(T,2));

T_sum = cumsum(T2); % cumulative sum

s_source_data{1} = source_data(:,1:T_sum(1));
s_statemat{1} = statemat(:,1:T_sum(1));
for t = 2:size(T2,2)
    s_statemat{t}       = statemat(:,T_sum(t-1)+1:T_sum(t));
    tmp                 = source_data(:,T_sum(t-1)+1:T_sum(t));
    tmp                 = filterdata(tmp',T2(t),fs,[1,30])';
    tmp                 = standardisedata(tmp',T2(t),1)';
    s_source_data{t}    = tmp;
end
clear tmp
%% Mean per state per subject
states_s_source_data = {};
mn_states_source_data = {};
for k = 1:nstates
    for t = 1:size(T2,2)
        indx = find(s_statemat{t}(k,:));
        states_s_source_data{k,t} = s_source_data{t}(:,indx);
        mn_states_source_data{k,t} = mean(s_source_data{t}(:,indx),2);
    end
end

%% Calculate threshold
dist = cell2mat(mn_states_source_data);
%histogram(dist(:),100)
%violin([reshape(dist(:,antagonist),1,[]); reshape(dist(:,placebo),1,[]); reshape(dist(:,agonist),1,[])]')
confidence = 50;
thrsh = prctile(dist(:),confidence,2);
% For 60% confidence.
% z_thrsh = 1; % 90% confidence
% confidence = 90;
% mn_state_betas = mean(state_betas,2);
% sd_state_betas = std(state_betas,0,2);
% thrs_betas = prctile(state_betas,confidence,2);
%% Surface plot with percentile threshold from the distribution of all subjects
for i = 1:3 % Antagonist Placebo Agonist
    for k = 1:size(s_beta,2) % States
        
        if i == 1
            condition = 'Antagonist';
        elseif i == 2
            condition = 'Placebo';
        elseif i == 3
            condition = 'Agonist';
        end
        
        %thrs = 1.28;%thrs = prctile(reshape(s_beta(cond_indx(i,:),k,:),1,[]),70);
        tmp = squeeze(mean(cell2mat(mn_states_source_data(k,cond_indx(i,:))),2));
        zero_indx = abs(tmp) <= thrs; % thresholding from general distribution
        tmp(zero_indx) = 0;
        betas = tmp(tmp ~= 0);
        areas = dkatlas.tissuelabel(tmp ~= 0)';
        s_table = table(areas, betas);
        writetable(s_table, ['output/areas/' condition '/state_areas_' num2str(k) '_60percentileThrs.txt'])
        sourceint.betas = tmp;
        cfg              = [];
        cfg.method       = 'surface';
        cfg.funparameter = 'betas';
        

        %cfg.maskparameter = cfg.funparameter;
        %cfg.funcolorlim   = [-0.01 0.01];
        % cfg.opacitylim    = [3 8];
        %cfg.opacitymap    = 'rampup';

        cfg.colorbar     = 'yes';
        cfg.funcolormap  = '*RdBu';
        figure;ft_sourceplot(cfg,sourceint);
        view([-90 30]);
        %light('style','infinite','position',[0 -200 200]);
        material dull
        set(gcf,'color','w');
        
        savefig(['output/figures/' condition '/meanState_surface_' num2str(k) '.fig']);
        close all
        
        % Plot from 4 points of view
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([-90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_1_60percentileThrsh.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([90 15]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_2_60percentileThrsh.png'])
        
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([180 0]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_3_60percentileThrsh.png'])
        
        cfg.colorbar     = 'no';
        ft_sourceplot(cfg,sourceint);
        material dull
        set(gcf,'color','w');
        view([0 0]);
        saveas(gcf, ['output/figures/pngs/' condition '/state' num2str(k) '_surface_view_4_60percentileThrsh.png'])
        
        close all
        
    end
end
clear tmp atmp ptmp percentage betas